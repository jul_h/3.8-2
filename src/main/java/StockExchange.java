import java.util.List;
import java.util.Map;

public class StockExchange {
    private Map<String, Share> shares;
    private List<Buyer> buyers;
    private boolean running;

    public StockExchange(Map<String, Share> shares, List<Buyer> buyers) {
        this.shares = shares;
        this.buyers = buyers;
    }

    public void start(int time) {
        running = true;

        for (Share share : shares.values()) {
            new Thread(() -> {
                while (running) {
                    share.updatePrice();
                    try {
                        Thread.sleep(30 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        for (Buyer buyer : buyers) {
            new Thread(() -> {
                while (running) {
                    buyer.buyShares(shares);
                    try {
                        Thread.sleep(5 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        try {
            Thread.sleep(time * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        running = false;
    }

    public void printReport() {
        System.out.println("Кінцева вартість акцій:");
        for (Share share : shares.values()) {
            System.out.println(share.getName() + ": " + share.getPrice());
        }
        System.out.println("\nЗвіт по покупцям:");
        for (Buyer buyer : buyers) {
            buyer.printReport();
        }
    }
}
