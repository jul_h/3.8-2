import java.time.LocalDateTime;

public class Share {
    private final String name;
    private int amount;
    private int price;

    public Share(String name, int amount, int price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public int getPrice() {
        return price;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void updatePrice() {
        double change = Math.random() * 0.03 * price;
        price += (Math.random() < 0.5) ? change : -change;
        System.out.println(LocalDateTime.now() + " Ціна акцій компанії " + name + " змінилась. Поточна вартість: " + price);
    }

    @Override
    public String toString() {
        return "Share{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                '}';
    }
}


