import java.time.LocalDateTime;
import java.util.*;

public class StockExchangeSimulator {
    public static void main(String[] args) {
      // Set of shares
      Map<String, Share> shares = new HashMap<>();
      shares.put("AAPL", new Share("AAPL", 100, 141));
      shares.put("COKE", new Share("COKE", 1000, 387));
      shares.put("IBM", new Share("IBM", 200, 137));
      // Set of buyers
      List<Buyer> buyers = new ArrayList<>();
      buyers.add(new Buyer("Alice", Arrays.asList(
              new Share("AAPL", 10, 100),
              new Share("COKE", 20, 390)
      )));
      buyers.add(new Buyer("Bob", Arrays.asList(
              new Share("AAPL", 10, 140),
              new Share("IBM", 20, 135)
      )));
      buyers.add(new Buyer("Charlie", List.of(
              new Share("COKE", 300, 370)
      )));

      StockExchange stockExchange = new StockExchange(shares, buyers);
      stockExchange.start(10 * 60);

      System.out.println("\n" + LocalDateTime.now() + " Біржу акцій закрито\n");
      stockExchange.printReport();
    }
}
