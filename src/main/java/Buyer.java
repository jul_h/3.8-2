import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Buyer {
    private final String name;
    private List<Share> buyersShares;
    private Map<String, Integer> purchases;

    public Buyer(String name, List<Share> buyersShares) {
        this.name = name;
        this.buyersShares = buyersShares;
        this.purchases = new HashMap<>();
    }

    public void buyShares(Map<String, Share> shares) {
        for (Share buyersShare : buyersShares) {
            Share share = shares.get(buyersShare.getName());
            int amount = buyersShare.getAmount();
            int price = buyersShare.getPrice();

            if (share.getAmount() >= amount && share.getPrice() <= price) {
                share.setAmount(share.getAmount() - amount);
                purchases.put(share.getName(), purchases.getOrDefault(share.getName(), 0) + amount);
                System.out.println(LocalDateTime.now() + " Спроба купівлі акцій " + share.getName() + " для " + name + " успішна. Куплено " + amount + " акцій за ціною " + price + ".");
            } else {
                System.out.println(LocalDateTime.now() + " Спроба купівлі акцій " + share.getName() + " для " + name + " не успішна.");
            }
        }
    }

    public void printReport() {
        System.out.println("\nЗвіт для " + name + ":");
        for (Map.Entry<String, Integer> entry : purchases.entrySet()) {
            System.out.println("Придбано акцій: " + entry.getValue() + " " + entry.getKey());
        }
    }
}
